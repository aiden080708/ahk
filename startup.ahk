﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

IfWinNotExist, gp5.ahk
	Run, gp5\gp5.ahk

IfWinNotExist, maxi-switch-keyboard.ahk
	Run, keyboard\maxi-switch-keyboard.ahk

IfWinNotExist, spam-click.ahk
	Run, spam-click\spam-click.ahk

IfWinNotExist, key-holder.ahk
	Run, key-holder\key-holder.ahk

IfWinNotExist, screen-zoomer.ahk
	Run, screen-zoomer\screen-zoomer.ahk

IfWinNotExist, wasd-alt-arrows.ahk
	Run, wasd-alt-arrows\wasd-alt-arrows.ahk

; IfWinNotExist, desktop-icons-toggle.ahk
; 	Run, desktop-icons-toggle.ahk